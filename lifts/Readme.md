## Lab #1. Concurrency

**Task:** simulate escalators/lifts in a building.

## Approach

We use multi-threading to manage concurrency.

## How to run


To run the simulation please look at `example.rb` and run commands in
`irb` console.

Logs are written to `lifts.log` file. Use `tail -f lifts.log` command to watch logs.
Default log level is *info*. Please change it in surce code (`lists_controller.rb`) to *debug* to see all the messages.
