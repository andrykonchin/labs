class Scheduler
  def initialize(called_floors, lifts, logger)
    @called_floors = called_floors
    @lifts = lifts
    @logger = logger
  end

  def call
    schedule_called_floors
    process_lifts_reached_target
    schedule_next_targets
  end

  private

  def schedule_called_floors
    @called_floors.each do |floor|
      index = rand(@lifts.size)
      @lifts[index].register_target(floor)

      @logger.debug "==== scheduler: assign called floor #{floor} to lift##{@lifts[index].id}"
    end
  end

  def process_lifts_reached_target
    @lifts.each do |lift|
      if lift.target_floor == lift.current_floor
        lift.deregister_target(lift.target_floor)
        lift.reset_target

        @logger.info "===== scheduler: lift##{lift.id} reached the target floor #{lift.current_floor}"
      end
    end
  end

  def schedule_next_targets
    @lifts.each do |lift|
      unless lift.target_floors.empty?
        target_floor = lift.target_floors.min { |a, b| lift.distance_to(a) <=> lift.distance_to(b) }

        if target_floor != lift.target_floor
          lift.set_target(target_floor)

          @logger.info "==== scheduler: lift##{lift.id} changes target floor #{target_floor}, among #{lift.target_floors}, current floor #{lift.current_floor}"
        end
      end
    end
  end
end
