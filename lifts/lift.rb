class Lift
  attr_reader :id
  attr_reader :current_floor
  attr_reader :target_floor
  attr_reader :target_floors

  def initialize(id, logger)
    @id = id
    @logger = logger
    @current_floor = 1
    @targer_floor = nil
    @target_floors = []

    @logger.debug "==== init lift##{@id}"
  end

  def register_target(floor)
    @target_floors << floor

    @logger.debug "==== lift##{id}: add target #{floor}. All the targets #{@target_floors}"
  end

  def deregister_target(floor)
    @target_floors.delete(floor)

    @logger.debug "==== lift##{id}: remove target #{floor}. All the targets #{@target_floors}"
  end

  def set_target(floor)
    @target_floor = floor

    @logger.debug "==== lift##{id}: new target #{floor}"
  end

  def reset_target
    @target_floor = nil
  end

  def move_towards_target
    if @target_floor && @target_floor != @current_floor
      if @target_floor > @current_floor
        @current_floor += 1

        @logger.info "==== lift##{id}: up to #{@current_floor}"
      else
        @current_floor -= 1

        @logger.info "==== lift##{id}: down to #{@current_floor}"
      end
    end
  end

  def distance_to(floor)
    (@current_floor - floor).abs
  end
end
