require 'logger'

require_relative 'lift'
require_relative 'building'
require_relative 'scheduler'

class LiftsController
  LIFT_FLOOR_TIME = 10
  SCHEDULER_TIC_TIME = 1

  def initialize(building)
    @mutex = Mutex.new
    @logger = Logger.new('lifts.log')
    @logger.level = Logger::INFO

    @building = building
    @lifts = (1 .. @building.lifts_number).map { |id| Lift.new(id, @logger) }
    @called_floors = []
  end

  def call_to(floor)
    raise ArgumentError, "Wrong floor number #{floor}" unless (1 .. @building.floors_number).cover?(floor)

    @mutex.synchronize do
      @called_floors << floor
    end
  end

  def lift_to(lift_id, floor)
    raise ArgumentError, "Wrong floor number #{floor}" unless (1 .. @building.floors_number).cover?(floor)
    raise ArgumentError, "Wrong lift id #{lift_id}" unless (1 .. @building.lifts_number).cover?(lift_id)

    @mutex.synchronize do
      lift_by(lift_id).register_target(floor)
    end
  end

  def run
    @stoped = false

    @lifts.each do |lift|
      Thread.new(lift) do |lift|
        until stoped? do
          @logger.debug "= lift##{lift.id} tic"

          @mutex.synchronize do
            lift.move_towards_target
          end
          sleep(LIFT_FLOOR_TIME)
        end

        @logger.info "==== controller: lift##{lift.id} stopped"
      end
    end

    Thread.new do
      until stoped? do
        @logger.debug "= scheduling tic"
        @mutex.synchronize do
          schedule_lifts
        end
        sleep(SCHEDULER_TIC_TIME)
      end

      @logger.info "==== controller: stopped"
    end
  end

  def stoped?
    @stoped
  end

  def stop
    @stoped = true
  end

  private

  def schedule_lifts
    Scheduler.new(@called_floors, @lifts, @logger).call
    @called_floors = []
  end

  def lift_by(id)
    @lifts.find { |lift| lift.id == id }
  end
end
