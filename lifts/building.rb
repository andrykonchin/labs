class Building
  attr_reader :floors_number
  attr_reader :lifts_number

  def initialize(floors_number, lifts_number)
    @floors_number = floors_number
    @lifts_number = lifts_number
  end
end
