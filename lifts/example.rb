require_relative 'lifts_controller'

c = LiftsController.new(Building.new(9, 1))

c.run

c.call_to(9)
c.call_to(5)

# ...

c.call_to(5)
c.lift_to(1, 4)
c.lift_to(2, 3)

c.stop
